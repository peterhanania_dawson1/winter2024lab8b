import java.util.Random;

public class Board {
    private Tile[][] grid;
    private final int size = 5;

    public Board() {
        grid = new Tile[size][size];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
            }
        }

        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int x = random.nextInt(size);
            int y = random.nextInt(size);
            grid[x][y] = Tile.HIDDEN_WALL;
        }

    }
    
    public int placeToken(int row, int col) {
        if (row < 0 || row >= size || col < 0 || col >= size) {
            return -2;
        }
        switch (grid[row][col]) {
            case WALL:
            case CASTLE:
                return -1;
            case HIDDEN_WALL:
                grid[row][col] = Tile.WALL;
                return 1;
            case BLANK:
                grid[row][col] = Tile.CASTLE;
                return 0;
            default:
                return -2;
        }
    }

    public String toString() {
        String result = "";

        for (Tile[] row : grid) {
            for (Tile tile : row) {
                result += tile.getName() + ' ';
            }
            result += '\n';
          
        }
        return result;
    }
}
