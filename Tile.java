public enum Tile {
    BLANK("_"),
    WALL("W"),
    HIDDEN_WALL("_"),
    CASTLE("C");

    private final String name;

    Tile(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
