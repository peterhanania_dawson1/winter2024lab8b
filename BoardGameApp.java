import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Board board = new Board();

        int castles = 5;
        int turns = 0;
        
        System.out.println("Welcome to the game");
        
        // 1-7
        while (castles > 0 && turns < 8) {
        
            // old board
            System.out.println(board);

            // left and the turn
            System.out.println("left to place: " + castles);
            System.out.println("turn: " + (turns + 1));
            
            // enter row and col to place castle
            System.out.println("Enter row and column to place a castle:");
            int row = scanner.nextInt();
            int col = scanner.nextInt();
            

            int result = board.placeToken(row, col);

            switch(result){
                case -2: 
                    System.out.println("invalid row/col");
                break;
                case -1: 
                    System.out.println("there is a castle already here");
                break;
                case 1:
                    turns++;
                    System.out.println("there is a wall here, so you will lose a turn");
                break;
                case 0:
                    turns++;
                    castles++;
                    System.out.println("your tile was placed successfully");
                break;
            }
            
        }
        
        System.out.println(board);

        if (castles == 0) {
            System.out.println("congrats you won the game");
        } else {
            System.out.println("you lose, no turns anymore");
        }
        
        scanner.close();
    }
}
